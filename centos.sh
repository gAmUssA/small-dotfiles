#!/bin/bash

set -e

if grep -iq centos /etc/issue; then
    yum -y install git vim
fi

mkdir -p .ssh

pushd .ssh
	if [ ! -f bitbucket_rsa ]; then
		echo "Name this one bitbucket_rsa"
		ssh-keygen -t rsa -C "kburke:$HOSTNAME -> bitbucket"
	fi
popd

mkdir -p ~/.dotfiles

if [ ! -f ~/.dotfiles/bitbucket-ssh ]; then
	echo "Copy this key to bitbucket:"
	cat .ssh/bitbucket_rsa.pub
	echo -e "\nPress any key to continue"
	read -n 1 -s
fi

touch ~/.dotfiles/bitbucket-ssh

# This is necessary because otherwise our ssh config doesn't know to use the 
# bitbucket SSH key
if [ ! -d small-dotfiles ]; then
    curl https://bitbucket.org/kevinburke/small-dotfiles/raw/master/.ssh/config --output .ssh/config
    git clone git@bitbucket.org:kevinburke/small-dotfiles.git
    rm .ssh/config
fi

pushd small-dotfiles
	cp -na . ..
popd

# Install other SSH keys
pushd .ssh
	if [ ! -f github_rsa ]; then
		echo "Name this one github_rsa"
		ssh-keygen -t rsa -C "kburke:$HOSTNAME -> github"
	fi
popd

if [ ! -f ~/.dotfiles/github-ssh ]; then
	echo "Copy this key to github:"
	cat .ssh/github_rsa.pub
	echo -e "\nPress any key to continue"
	read -n 1 -s
fi

touch ~/.dotfiles/github-ssh

### Install latest git ###
if [ ! -f $HOME/bin/git ]; then 
    sh installers/git.sh
    # XXX hack
    mv /usr/bin/git /usr/bin/git-1.7 || true
fi

### ZSH ###
if [ ! -f /usr/local/bin/zsh ]; then
    sh installers/zsh.sh
fi

sudo echo "/usr/local/bin/zsh" >> /etc/shells
sudo chsh -s /usr/local/bin/zsh

if [ ! -d ~/.oh-my-zsh ]; then
    git clone git://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
fi

### VIM ###
mkdir -p .vim/tmp/undo .vim/tmp/backup .vim/tmp/swap

git submodule update --init

mkdir -p .vim/autoload
cp .vim/pathogen/autoload/pathogen.vim .vim/autoload

## Source code
mkdir -p ~/code ~/share
pushd ~/code
    if [ ! -d gitopen ]; then
        git clone git@github.com:kevinburke/gitopen.git
    fi
    if [ ! -d weirdfortune ]; then
        git clone git@github.com:kevinburke/weirdfortune.git
    fi
    if [ ! -d solarized ]; then
        git clone git@github.com:altercation/solarized.git
    fi
    if [ ! -d better-hn-filter ]; then
        if [ `uname` = "Darwin" ]; then
            git clone git@bitbucket.org:kevinburke/better-hn-filter.git
        fi
    fi
popd

pushd ~/code/weirdfortune
    make install
popd

sh installers/autojump.sh
sh installers/silversearcher.sh

mkdir -p ~/code/go
echo "checking out Go source code"
if [ ! -f ~/code/go/src/code.google.com/p/go ]; then 
    go get code.google.com/p/go
fi

if [ ! -f ~/code/local-servers ]; then 
    if [ `uname` == "Darwin" ]; then
        git clone https://github.com/kevinburke/local-servers.git ~/code/local-servers
        pushd ~/code/local-servers
        make install
    fi
fi
