#!/usr/bin/env bash

set -eo pipefail
set -x

VERSION='1.7.4'
ROOT="$HOME/go1.7"
DEVEL="$HOME/go"
# TODO uname matching
BINARY="go$VERSION.darwin-amd64.tar.gz"
URL="https://storage.googleapis.com/golang/$BINARY"

install_go() {
    if [[ -f "$ROOT/bin/go" ]]; then
        return 0
    fi
    mkdir -p ~/tmp
    pushd ~/tmp
        if [[ ! -f "$BINARY" ]]; then
            curl --location -O "$URL"
        fi
        if [[ ! -d "$DIRECTORY" ]]; then
            tar -xf "$BINARY"
        fi
        mv go "$ROOT"
    popd
}

install_go_tip() {
    if [[ -f "$DEVEL/bin/go" ]]; then
        return 0
    fi
    git clone https://go.googlesource.com/go "$DEVEL"
    pushd "$DEVEL/src"
        GOROOT_BOOTSTRAP="$ROOT" ./make.bash
    popd
}

# Vim settings
install_vim_settings() {
    mkdir -p $HOME/.vim/ftdetect
    mkdir -p $HOME/.vim/syntax
    mkdir -p $HOME/.vim/autoload/go
    if [ ! -L $HOME/.vim/ftdetect/gofiletype.vim ]; then
        ln -s $ROOT/misc/vim/ftdetect/gofiletype.vim $HOME/.vim/ftdetect/
    fi
    if [ ! -L $HOME/.vim/syntax/go.vim ]; then
        ln -s $ROOT/misc/vim/syntax/go.vim $HOME/.vim/syntax
    fi
    if [ ! -L $HOME/.vim/autoload/go/complete.vim ]; then
        ln -s $ROOT/misc/vim/autoload/go/complete.vim $HOME/.vim/autoload/go
    fi
}

main() {
    install_go
    install_go_tip

    # install_vim_settings
}

main "$@"
