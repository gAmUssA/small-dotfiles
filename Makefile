.PHONY: hosts update-submodules

update-submodules:
	git submodule foreach git pull origin master

hosts:
	go run scripts/ipv6-hostsfile.go|pbcopy
