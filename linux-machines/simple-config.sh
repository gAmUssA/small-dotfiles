#!/bin/bash

install() {
	sudo apt-get update
	sudo apt-get install vim
    sudo apt-get install git
    sudo apt-get install mercurial
	bash installers/golang.sh
}

main() {
    mkdir -p "$HOME/go"
	export GOPATH="$HOME/go:$GOPATH"
    export GOROOT="/usr/local/go"
	export PATH="$GOROOT/bin:$HOME/go/bin:$PATH"
}
main
