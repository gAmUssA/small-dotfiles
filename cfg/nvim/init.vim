call pathogen#infect('~/cfg/nvim/bundle/{}')
syntax on

filetype plugin indent on

if &encoding != "utf-8"
    set encoding=utf-8
endif

set termguicolors

set modelines=0
highlight MatchParen ctermbg=4
set laststatus=2
"set statusline=%F%m%r%h%w%=(%{&ff}/%Y)\ (line\ %l\/%L,\ col\ %c)
set statusline=%<%f\ (%{&ft})\ %-4(%m%)%=%-19(%3l,%02c%03V%)
set noerrorbells
set cursorline
set history=1000
set scrolloff=8
set sidescrolloff=5
set autoindent
set showmode
set splitright
set showcmd
set hidden
set wildmenu
set wildmode=list:longest
set wildignore+=*.hi,*.pyc,*.class,*.o,*.dSYM,*.dSYM/,*.egg-info,*.egg-info/,venv,build,cover,_build,include,Godeps,node_modules
set wildignorecase
set visualbell
set ruler
set backspace=indent,eol,start
set laststatus=2
set lazyredraw
set timeout timeoutlen=1000 ttimeoutlen=10
set nojoinspaces
set autoread

"" default tab settings - should override these for specific extensions (HTML,
"" etc)
set tabstop=4
set shiftwidth=4
set softtabstop=4
if has("autocmd")
    " don't judge
    autocmd BufEnter *.coffee,*.js set softtabstop=2
    autocmd BufEnter *.coffee,*.js set tabstop=2
    autocmd BufEnter *.coffee,*.js set shiftwidth=2
endif
set expandtab
set wrap
set textwidth=79
" c = auto wrap comments on 79 line length
set formatoptions=cqrn1

"" search settings
nnoremap / /\v
vnoremap / /\v
set ignorecase
set smartcase
set gdefault
set incsearch
set showmatch
set hlsearch

if version >= 703
    set colorcolumn=80
    hi ColorColumn ctermbg=lightgrey guibg=#cccccc
    "set undofile
endif

"" gather all .swp, .un~ in same directory
if version >= 703
    set undodir=~/cfg/vim/tmp/undo/
    set backupdir=~/cfg/vim/tmp/backup/
    set directory=~/cfg/vim/tmp/swap/
endif

if has("signs") && version >= 703
    set relativenumber
endif

"" show line endings
set list
set listchars=tab:+\ ,eol:¬
set formatprg=par\ -w79\ -T4

" This doesn't work well, but I'm not sure why. 
" http://vi.stackexchange.com/a/6966/7848
tnoremap <Esc> <C-\><C-n>

nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>
nnoremap j gj
nnoremap k gk
map <C-k> <C-w><Up>
map <C-j> <C-w><Down>
map <C-l> <C-w><Right>
map <C-h> <C-w><Left>
inoremap <F1> <ESC>
nnoremap <F1> <ESC>
vnoremap <F1> <ESC>
inoremap jj <ESC>
imap <c-c> <esc>
cnoremap %% <C-R>=expand('%:h').'/'<cr>
map <leader>e :edit %%

nnoremap Q @='n.'<CR>

"" one less key for write
nnoremap ; :

"" matchit remapping
nmap <tab> %
vmap <tab> %

"" end of line
nnoremap L $
vnoremap L $

"" center after jump
nnoremap n nzz
nnoremap N Nzz

let mapleader = ","

"" clear a search
nnoremap <leader><space> :nohlsearch<cr>

nnoremap <leader>c gg"+yG<cr>
vnoremap <leader>c :CoffeeCompile<cr>

" flush commandt results
nnoremap <leader>f :CtrlPClearCache<cr>

function! GoSyntaxCheck()
    if (match(expand("%"), "test") != -1) 
        :GoTestCompile
    else
        :GoBuild
    endif
endfunction

nnoremap <leader>d :call GoSyntaxCheck()<CR>

if (match(getcwd(), "code/api") != -1)
  let g:CommandTFileScanner = "git"
endif

set ttimeoutlen=50

if &term =~ "xterm" || &term =~ "screen"
  let g:CommandTCancelMap = ['<ESC>', '<C-c>']

  " when I originally started using Command-T inside a terminal,
  " I used to need these as well:
  let g:CommandTSelectNextMap = ['<C-j>', '<ESC>OB']
  let g:CommandTSelectPrevMap = ['<C-k>', '<ESC>OA']
endif

let g:go_test_timeout = '2s'
nnoremap <leader>g :GoTest<cr>

nnoremap <leader>r :!test-files-in-diff \| xargs tt<CR>
nnoremap <leader>n :call WriteAndRunTest()<CR>

let test#strategy = "neovim"
let g:test#preserve_screen = 1
let test#javascript#mocha#executable = 'tt2 --bail'

let test#php#runner = './bin/run-tests'
let test#php#phpunit#executable = './bin/run-tests'

function! RunFile()
    if (&ft == 'python')
      exec ":!source venv/bin/activate; python " . bufname('%')
    endif
endfunction

function! WriteAndRunTest()
    " save the file first
    if expand("%") != ""
      if &ft=='go'
      else
        :w
      endif
    end
    if &ft=='go'
      :GoTestFunc
    else
      :TestNearest
    endif
endfunction

function! RunTest()
    if (&ft=='python')
        execute "normal" ?def test_<CR>wyw
    endif
endfunction

nnoremap <leader>o :call ReloadChrome()<CR>:pwd<cr>

"" visually select last edited text
nmap gV `[v`]

set background=dark
"highlight Normal ctermfg=grey ctermbg=black
colorscheme base16-ateliersavanna

"" autosave on lose focus
"au FocusLost * :wa

""to avoid crontab edit errors"
set backupskip=/tmp/*,/private/tmp/*,*.zip


if has('cmdlog')
  " cmdlogdir:
  "     The directory which will be used to store logs.
  set cmdlogdir=~/.vimlogs/
  "" cmdloginsert:
  ""     Log text entered in normal mode.
  ""     Disabled by default
  set cmdloginsert
end

if has("autocmd")
    autocmd BufWritePre * :call StripEndingNewlines()

    "" delete trailing whitespace when you save a file
    autocmd BufWritePre *.php :call StripTrailingWhitespaces()
    autocmd BufWritePre *.py :call StripTrailingWhitespaces()
    autocmd BufWritePre *.rb :call StripTrailingWhitespaces()
    autocmd BufWritePre *.js :call StripTrailingWhitespaces()
    autocmd BufWritePre *.txt :call StripTrailingWhitespaces()
    autocmd BufWritePre *.hs :call StripTrailingWhitespaces()
    autocmd BufWritePre *.rb :call StripTrailingWhitespaces()
    autocmd BufWritePre *.java :call StripTrailingWhitespaces()
    autocmd BufWritePre *.md :call StripTrailingWhitespaces()
    autocmd BufWritePre *.markdown :call StripTrailingWhitespaces()
    autocmd BufWritePre *.coffee :call StripTrailingWhitespaces()
    autocmd BufWritePre *.bash :call StripTrailingWhitespaces()
    autocmd BufWritePre *.json :call StripTrailingWhitespaces()
    autocmd BufWritePre *.html :call StripTrailingWhitespaces()
    autocmd BufWritePre *.yml :call StripTrailingWhitespaces()
    autocmd BufWritePre *.conf :call StripTrailingWhitespaces()
    autocmd BufWritePre *.rules :call StripTrailingWhitespaces()

    autocmd BufRead,BufNewFile *.go set noexpandtab
    autocmd BufRead,BufNewFile *.go set filetype=go
    autocmd BufRead,BufNewFile *.go setlocal tabstop=4 softtabstop=4


    " auto wrap .txt files to 80 characters
    autocmd BufRead,BufNewFile *.txt set formatoptions+=t

    " except for python.
    autocmd BufRead,BufNewFile *.py,*.md,*.scss,*.conf,*.conf.disabled set expandtab
    autocmd BufRead,BufNewFile *.css set expandtab

    autocmd BufRead,BufNewFile *.php.mock set ft=php
    autocmd BufRead,BufNewFile *.proto setfiletype proto

    autocmd BufRead,BufNewFile *.mustache set ft=html syntax=html
    autocmd BufRead,BufNewFile *.phtml set ft=html syntax=html

    "" for my compilers class
    autocmd BufRead,BufNewFile *.bcs set filetype=cs

    " detect text filetype
    autocmd BufEnter * if &filetype == "" | setlocal ft=txt | endif

    autocmd FileType nginx setlocal shiftwidth=4 tabstop=4 softtabstop=4
    autocmd FileType go setlocal tabstop=4 softtabstop=4
    autocmd FileType proto setlocal shiftwidth=2 tabstop=2 softtabstop=2

    autocmd FileType html set filetype=htmldjango
    autocmd FileType html set expandtab
    autocmd FileType mustache set filetype=htmldjango
    autocmd FileType mako set filetype=mako
    autocmd FileType modula2 set filetype=markdown

    autocmd FileType haml,html,ruby setlocal shiftwidth=2 tabstop=2 softtabstop=2


    let g:gist_post_private = 1
endif

let g:go_fmt_command = "goimports"
let g:go_list_type = "quickfix"

function! StripTrailingWhitespaces()
    " save last search & cursor position
    let _s=@/
    let l = line(".")
    let c = col(".")
    %s/\s\+$//e
    let @/=_s
    call cursor(l, c)
endfunction

function! StripEndingNewlines()
    " save last search & cursor position
    let _s=@/
    let l = line(".")
    let c = col(".")
    " remove ending whitespace: http://stackoverflow.com/a/7496112/329700, 
    " silent on error: http://stackoverflow.com/a/1043613/329700
    %s/\($\n\s*\)\+\%$//e
    let @/=_s
    call cursor(l, c)
endfunction

if has("gui_running")
    set guioptions=egmrt
endif

"" look for a tags file, starting in current dir and working up to root
set tags=tags;/

let CommandTMaxFiles = 40000

set rtp+=/Users/kevin/code/go/misc/vim

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_python_pylint_args ='--disable=line-too-long'

" https://github.com/fatih/vim-go#using-with-syntastic
let g:syntastic_go_checkers = ['golint', 'govet', 'errcheck']
let g:syntastic_mode_map = { 'mode': 'active', 'passive_filetypes': ['go'] }

let g:ctrlp_map = '<leader>t'
let g:ctrlp_cmd = 'CtrlP'
map <leader>b :CtrlPBuffer<CR>
let g:ctrlp_working_path_mode = 'ra'

" https://github.com/neovim/neovim/issues/2048
if has('nvim')
    nmap <BS> <C-W>h
endif
